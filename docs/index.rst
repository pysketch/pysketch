.. pysketch documentation master file, created by
   sphinx-quickstart on Sun Feb 14 16:11:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysketch
====================================

Pysketch is a program-synthesis front-end that works with Python source-code.

**Pysketch is in very early stages of development, use by end-users is discouraged.**

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   devs

Documentation:

.. autosummary::
   :toctree: _autosummary

   pysketch.synthesizers
   pysketch.types


Quickstart
----------

``$ pip install --user pysketch``

.. literalinclude:: ../examples/helloworld.py

Source code
-----------

* https://gitlab.com/pysketch/pysketch


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

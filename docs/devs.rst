Developer resources
====================================

Simplest virtual environment setup
----------------------------------

1. Install poetry:

.. code-block:: bash

  curl -ssl https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

2. Clone the git repository:

.. code-block:: bash

  git clone https://gitlab.com/pysketch/pysketch.git

3. Install dependencies and start virtual environment

.. code-block:: bash

  cd pysketch
  poetry install
  poetry shell

Developing pysketch as external dependency
------------------------------------------

A simple development setup using `poetry <https://python-poetry.org/>`_ is
described. Using this setup is not necessary, but provides a virtual
environment. An alternative is to use pip with the -e option.

1. install poetry:

.. code-block:: bash

  curl -ssl https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

2. clone the git repository:

.. code-block:: bash

  git clone https://gitlab.com/pysketch/pysketch.git

3. **In a different directory**, setup a virtual environment and add the
   pysketch repository as dependency:

.. code-block:: bash

  cd anotherdir
  poetry init                   # Accept default settings
  poetry add path/to/pysketch

4. Edit `pyproject.toml` so that pysketch is tagged as a development dependency:

.. code-block:: bash

  pysketch = {path = "path/to/pysketch", develop=True}

5. Start the virtual environment. Any changes to pysketch will be reflected
   inside the virtual environment.

.. code-block:: bash

  poetry shell   # Starts a shell in the virtual environment

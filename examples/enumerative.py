# Local imports
from pysketch.synthesizers import EnumerativeSynthesizer
from pysketch.types import Code
from pysketch.types import Constant

# Native imports
from typing import List
from itertools import product


synthesizer = EnumerativeSynthesizer()


# Example 1: Hello world
# We want the synthesizer to fill the code with a constant that satisfies
# the assertion for all inputs
# The synthesizer is able to read the type annotation, and thus, to form
# a set of inputs to evaluate the program on.
@synthesizer
def doubleSketch(x: int):
    t = x * Constant()
    assert t == x + x
    return t


print(synthesizer.sourcecode(doubleSketch))
print(doubleSketch(3))


# Example 2: input output examples
# We want the synthesizer to find a function F such that
#      F(x) == y
# for all the provided (x, y) pairs
@synthesizer
@synthesizer.io_pairs(*[
        ((x, y), 2*x + y + 2)
        for x, y in product(range(100), range(100))
    ])
def linexp(x: int, y: int):
    return Constant()*x + Constant()*y + Constant()


print(synthesizer.sourcecode(linexp))
print(linexp(3, 5))


# Example 3: harness
# We want the synthesizer to complete the code in any way it can so that
# the function satisfies the assertion for all inputs.
@synthesizer
def f(A: List[int], k: int) -> int:
    if len(A) <= k:
        return 0
    result = 1000
    Code()
    assert result == A[0]
    return result


print(synthesizer.sourcecode(f))
print(f([0, 1, 2], 2))


# Example 5: real world example
# We want to open a CSV and extract the values in
# a particular column.
import numpy as np
@synthesizer
def parsecsv():
    csv = Code("open csv 'sample1.csv'")
    averages = csv[Constant():, Constant()]
    assert averages[0] == 0.1, averages[1] == 0.5
    return averages


print(synthesizer.sourcecode(parsecsv))
print(parsecsv())


# Example 4: natural language guided synthesis
# (only works if run on 2021)
import datetime
@synthesizer
@synthesizer.io_pairs(*[
        (([4, 2, 6, 8, 3], 10), 2*10*2021),
        (([1, 2, 0, 4, 7], 10), 0),
    ])
def multilinef(l1: List[int], k: int) -> int:
    mi = Code("minimum of 'l1'")
    year = Code("current year")
    return mi*k*year


print(synthesizer.sourcecode(multilinef))
print(multilinef([4, 1, 2], 2))


##@synthesizer
##def f(A: List[int], k: int) -> List[int]:
##    if len(A) > k:
##        Code()
##
##    while Bool("the length of A is less than k"):
##        A.append(0)
##
##    assert len(A) == k
##
##    return A

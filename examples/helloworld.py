from pysketch.synthesizers import EnumerativeSynthesizer
from pysketch.types import Constant

synthesizer = EnumerativeSynthesizer()

# Example 1: Hello world
# We want the synthesizer to fill the code with a constant that satisfies
# the assertion for all inputs
# The synthesizer is able to read the type annotation, and thus, to form
# a set of inputs to evaluate the program.
@synthesizer
def doubleSketch(x: int):
    t = x * Constant()
    assert t == x + x
    return t


print(synthesizer.sourcecode(doubleSketch))
print(doubleSketch(3))

"""Source code hole types."""


class SketchType:
    """Type abstract base class."""
    def __init__(self, *args, **kwargs):
        pass


class Code(SketchType):
    """The most general type, meant to represent arbitrary holes in source
    code."""
    def __init__(self, utterance: str):
        """
        Parameters
        ----------

        - utterance
            Natural language annotation.
        """
        pass


class Bool(SketchType):
    """Boolean type, meant to represent boolean holes."""
    pass


class Constant(SketchType):
    """Boolean type, meant to represent constant holes."""
    pass


type_ids = ["Code", "Bool", "Constant"]

"""Defines the Synthesizer abstract base class."""
# Native imports
import abc
from typing import Callable
from typing import Tuple


class Synthesizer(abc.ABC):
    """Defines the interface and core functionality of all synthesizer
    classes. All instances of this class can be used as decorators."""

    @abc.abstractmethod
    def synthesized(self, f: Callable) -> Tuple[Callable, str]:
        """Receives a python function and returns a new function and the new
        source code. To implement a new synthesizer this method must be
        implemented."""
        pass

    def __init__(self):
        self.synthesized_fs = dict()
        self.sourcecodes = dict()

    def __call__(self, inp):
        # Hacky implementation. If we receive a tuple
        # we assume we are receiving iopairs as well.
        if isinstance(inp, tuple):
            f, iopairs = inp
        else:
            f = inp
            iopairs = None
        sf, _ = self._synthesized(f, iopairs=iopairs)
        return sf

    def io_pairs(self, *iopairs):
        """Adds an input-output pairs constraint to a function to be
        synthesized.

        Parameters
        ----------
        iopairs: list[tuple]
            List of input-output examples that the function has to satisfy.
        """
        # Hacky implementation. function -> (iopairs, funcion)
        def f2(f):
            inp = (f, iopairs)
            return self(inp)
        return f2

    def _synthesized(self, f, iopairs=None):
        # See if we already computed the input
        if f.__qualname__ in self.synthesized_fs:
            return self.synthesized_fs[f.__qualname__], self.sourcecodes[f.__qualname__]

        synthesized_f, sourcecode = self.synthesized(
                f,
                iopairs=iopairs
                )

        # Store the output for future use
        self.synthesized_fs[f.__qualname__] = synthesized_f
        self.sourcecodes[f.__qualname__] = sourcecode
        return synthesized_f, sourcecode

    def sourcecode(self, f) -> str:
        _, sourcecode = self._synthesized(f)
        return sourcecode

# Local imports
from ..types import type_ids
from .synthesizer import Synthesizer

# Native imports
from typing import Set
from typing import List
from typing import Union
from typing import Callable
import ast
import inspect
import copy
import random
from itertools import product
import tokenize
import token
from collections import defaultdict
import urllib.request
import json

# External imports
from tqdm import tqdm


class IdSketchNodes(ast.NodeTransformer):
    """This class visits all the nodes of an ast node assigning "sketch nodes"
    type-specific ids, and stores their arguments.

    "Sketch nodes" are calls like Code() or Constant().  Code and Constant are
    examples of "sketch types".

    So this class would id the Code()s with e.g. [0,1,2,3,4] and the Constant()s
    with [0,1,2,3].
    """
    def __init__(self):
        self.type_to_ids = {typeid: list() for typeid in type_ids}
        self.type_to_id_to_args = {typeid: defaultdict(list) for typeid in type_ids}

    def visit_Call(self, node):
        if isinstance(node.func, ast.Name) and node.func.id in type_ids:
            id = max(self.type_to_ids[node.func.id] + [-1]) + 1
            idnode = ast.Constant(id)
            key = ast.keyword("id", idnode)
            node.keywords.append(key)
            self.type_to_ids[node.func.id].append(id)
            for arg in node.args:
                if isinstance(arg, ast.Constant):
                    self.type_to_id_to_args[node.func.id][id].append(arg.value)
                else:
                    self.type_to_id_to_args[node.func.id][id].append(arg)
        return node


class RewriteConstant(ast.NodeTransformer):
    """Replaces Constant()s with the given id, with the
    given constant.

    It is expected that the given constant can function as the value of
    ast.Constant. I.e. the given constant is a number, a string, None,
    or an immutable container.

    https://docs.python.org/3/library/ast.html#ast.Constant
    """
    def __init__(self, id, n):
        self.id = id
        self.n = n

    def visit_Call(self, node):
        if isinstance(node.func, ast.Name) and node.func.id == "Constant":
            id_match = False
            for kwarg in node.keywords:
                if kwarg.arg == "id" and isinstance(kwarg.value, ast.Constant) and kwarg.value.value == self.id:
                    id_match = True
            if id_match:
                inttree = ast.Constant(self.n)
                return inttree
        return node


class ReplaceCode(ast.NodeTransformer):
    """Replaces Code()s with the given id, with the
    given ast node or ast node list."""
    def __init__(self, id, newcode):
        self.id = id
        self.newcode = newcode

    def visit_Assign(self, node):
        # varname = Code()
        if isinstance(node.value, ast.Call) and isinstance(node.value.func, ast.Name) and node.value.func.id == "Code":
            id_match = False
            for kwarg in node.value.keywords:
                if kwarg.arg == "id" and isinstance(kwarg.value, ast.Constant) and kwarg.value.value == self.id:
                    id_match = True
            if id_match:
                if isinstance(self.newcode, ast.Module)\
                        and isinstance(self.newcode.body[0], ast.Expr):
                    node.value = self.newcode.body[0].value
                else:
                    node.value = self.newcode
        return node

    def visit_Expr(self, node):
        # Code()
        if isinstance(node.value, ast.Call) and node.value.func.id == "Code":
            id_match = False
            for kwarg in node.value.keywords:
                if kwarg.arg == "id" and isinstance(kwarg.value, ast.Constant) and kwarg.value.value == self.id:
                    id_match = True
            if id_match:
                return self.newcode
        return node


def is_tree_clean(tree) -> bool:
    """Returns True if and only if there is an ast.Name
    that corresponds to a sketch type."""
    for node in ast.walk(tree):
        if isinstance(node, ast.Name) and node.id in type_ids:
            return False
    return True


def get_variable_names(tree) -> Set[str]:
    """Returns all the ast.Name values (e.g. function names,
    variable names, etc.)."""
    names = set()
    for node in ast.walk(tree):
        if isinstance(node, ast.Name):
            names.add(node.id)
    return names


token_to_string = {
        token_type: token_name
        for token_name, token_type in
        token.EXACT_TOKEN_TYPES.items()
        }
#possible_tokens = token.tok_name.keys()
possible_tokens = {
        token.NAME, token.NUMBER, token.LSQB, token.RSQB, token.EQUAL, token.COLON,
        }
def _generate_code(
        length: int,
        names: Set[str],
        numbers: Set[Union[int, float]],
        strings: Set[str],
        ):
    if length == 0:
        yield []
    else:
        for code in _generate_code(length-1, names, numbers, strings):
            if len(code) > 0:
                next_token = code[0]
            else:
                next_token = None
            for token_type in possible_tokens:
                if next_token is not None and token_type == next_token[0]:
                    # We can't have e.g. "var var = 1 1 1"
                    continue
                if next_token is not None\
                        and token_type == token.NUMBER\
                        and (next_token[0] == token.LSQB or next_token[0] == token.LPAR):
                    # We can't have e.g. "10[2] = 3"
                    continue
                if next_token is not None\
                        and token_type == token.EQUAL\
                        and token.EQUAL in (c[0] for c in code):
                    # We don't want e.g. "A = 1 = 2"
                    continue
                if next_token is not None\
                        and token_type == token.NAME\
                        and next_token[0] == token.NAME:
                    # We don't want e.g. "A A" or "a = sum sum"
                    continue
                if next_token is not None\
                        and token_type == token.NUMBER\
                        and next_token[0] == token.NUMBER:
                    # We don't want e.g. "1 1"
                    continue
                if next_token is not None\
                        and token_type == token.NAME\
                        and next_token[0] == token.NUMBER:
                    # We don't want e.g. "A 1"
                    continue
                if next_token is not None\
                        and token_type == token.NUMBER\
                        and next_token[0] == token.NAME:
                    # We don't want e.g. "1 A"
                    continue
                currents = list()
                if token_type in token_to_string.keys():
                    currents = [(token_type, token_to_string[token_type])]
                elif token_type == token.NAME:
                    currents = ((token_type, name) for name in names)
                elif token_type == token.NUMBER:
                    currents = ((token_type, str(number)) for number in numbers)
                elif token_type == token.STRING:
                    currents = ((token_type, string) for string in strings)
                for current in currents:
                    yield (current, *code)


def generate_code(
        *args,
        max_length: int,
        line_n: int,
        ):
    """Dumb enumeration of all possible token sequences of the maximum
    number of lines and length."""
    line_lens = [range(1, max_length+1) for _ in range(line_n)]
    single_lines = [list(_generate_code(line_len, *args)) for line_len in range(1, max_length+1)]
    for line_lens_ in product(*line_lens):
        all_lines_ = [single_lines[i-1] for i in line_lens_]
        for lines_ in product(*all_lines_):
            tokens = list()
            for i, line_ in enumerate(lines_):
                tokens.extend(line_)
                if i < len(lines_)-1:
                    tokens.append((token.NEWLINE, "\n"))
            try:
                sourcecode = tokenize.untokenize(tokens)
                ast_ = ast.parse(sourcecode)
                #print(sourcecode)
            except Exception as e:
                continue
            yield ast_


def sample_args(f, k: int):
    """Returns a list of arguments based on the typing
    annotation of the given function.

    It is expected that the function exists in the sourcefile
    it was created in (i.e. it was not a custom AST).
    """
    sig = inspect.signature(f)
    args = list()
    unsupported_annotation = False
    for _ in range(k):
        _args = list()
        for param in sig.parameters.values():
            if param.annotation == List[int]:
                _len = random.randint(2, 100)
                inp = [random.randint(0, 100) for _ in range(_len)]
            elif param.annotation == int:
                inp = random.randint(0, 100)
            else:
                # Typing annotation not supported, or not given
                unsupported_annotation = True
            _args.append(inp)
        args.append(_args)
    if unsupported_annotation:
        return list()
    return args


def enumerative_synthesizer(
        f,
        iopairs=None,
        max_length=6,
        max_lines=1,
        ):
    """Fills the Sketch types in the given function by enumerating over token
    sequences, constant values and translation output, depending on the
    structure of the given function."""
    tree = ast.parse(inspect.getsource(f))

    # Build inputs
    test_suite = sample_args(f, 100)

    # Remove decorators
    tree.body[0].decorator_list = list()

    # Assign ids to all sketch nodes
    idr = IdSketchNodes()
    tree = idr.visit(tree)

    # Find Code()s natural language annotations
    code_args = {id: args for id, args in idr.type_to_id_to_args["Code"].items()}

    # Build a map (codeid) -> [list of possible asts]
    code_hypotheses = defaultdict(list)
    for codeid, codeargs in code_args.items():
        # tranX
        # if the Code() had a string as the first argument, ask tranX
        if len(codeargs) > 0 and isinstance(codeargs[0], str) and len(codeargs[0]) > 0:
            utterance = codeargs[0]
            url = "http://localhost:8081/parse/conala/{}".format(urllib.parse.quote(utterance))
            print("Requesting {}".format(url))
            try:
                contents = urllib.request.urlopen(url).read()
                contents = json.loads(contents)
                for hypothesis in contents['hypotheses']:
                    sourcecode = hypothesis['value']
                    # Avoid programs that print to stdout
                    if 'print' in sourcecode:
                        continue
                    code_hypotheses[codeid].append(ast.parse(sourcecode))
            except Exception as e:
                print("Contacting tranX failed, or returned invalid output")

        # Ask other code translators
        # ...

    # Identify variable names and other constants
    varnames = get_variable_names(tree)
    array_names = get_variable_names(tree)
    indices = set(range(3))
    smallintegers = list(range(3))

    # Possible values for Constant()s: small integers
    var_value_generator = tuple(product(*[smallintegers]*len(idr.type_to_ids["Constant"])))

    # Possible values for Code()'s
    names = varnames | array_names | {"append"}
    numbers = indices
    strings = "test"

    pbar = tqdm(desc="Enumerating programs")
    for line_n in range(0, max_lines+1):

        # In the first round only enumerate with the
        # translator suggestions. Then, expand the
        # the search with simple code enumeration.
        enumerated_lines = tuple()
        if line_n > 0 and len(idr.type_to_ids["Code"]) > 0:
            enumerated_lines = tuple(tqdm(generate_code(
                names, numbers, strings, max_length=max_length, line_n=line_n
                ),desc="Non-guided brute-force token sequence search. Lines, max line-length {}, {}".format(line_n, max_length)))
        code_value_generator = tuple(product(*[
            tuple(code_hypotheses[id]) + enumerated_lines
            for id in idr.type_to_ids["Code"]
            ]))

        # Enumerate all possible replacements
        for code_values, var_values in product(code_value_generator, var_value_generator):
            pbar.update()
            _tree = copy.deepcopy(tree)

            # Replace the holes with the current replacements
            for id, val in zip(idr.type_to_ids["Code"], code_values):
                _tree = ReplaceCode(id, val).visit(_tree)
            for id, val in zip(idr.type_to_ids["Constant"], var_values):
                _tree = RewriteConstant(id, val).visit(_tree)

            #print(ast.unparse(_tree))

            # Ensure no sketch primitives in tree
            if not is_tree_clean(_tree):
                continue

            # Try to compile
            try:
                _locals = dict()
                _globals = f.__globals__
                exec(ast.unparse(_tree), _globals, _locals)
                synthesized_f = _locals[f.__name__]
            except Exception:
                continue

            # Try to execute with test input
            try:
                for test_args in test_suite:
                    synthesized_f(*test_args)
                if iopairs is not None:
                    for x, y in iopairs:
                        y_ = synthesized_f(*x)
                        assert y == y_
            except Exception:
                continue

            # If execution succeeded on test input and iopairs,
            # assume function was found
            pbar.close()
            return synthesized_f, ast.unparse(_tree)
    pbar.close()
    raise Exception("Unable to synthesize {}".format(f.__qualname__))


class EnumerativeSynthesizer(Synthesizer):
    """Code-translation guided enumeration."""
    def synthesized(
            self,
            f: Callable,
            iopairs=None,
            max_length=6,
            max_lines=1,
            ) -> tuple[Callable, str]:
        """Code translation guided enumeration.

        Parameters
        ----------

        f: ~typing.Callable
            Function to synthesize.

        iopairs: list[tuple] or None
            Input-output pairs specification.

        max_length: int
            Maximum length in characters of each line for the new sourcecode.

        max_lines: int
            Maximum number of lines for each hole in the new sourcecode.
        """
        return enumerative_synthesizer(
                f,
                iopairs=iopairs,
                max_length=max_length,
                max_lines=max_lines,
                )
